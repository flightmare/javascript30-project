//declaration
const player = document.querySelector('.player');
const video = player.querySelector('.viewer');
const playButton = player.querySelector('button[title="Toggle Play"]');
const volume = player.querySelector('input[name="volume"]');
const playBackRate = player.querySelector('input[name="playbackRate"]');
const back10s = player.querySelector('button[data-skip="-10"]');
const skip10s = player.querySelector('button[data-skip="10"]');
const progress = player.querySelector('.progress__filled');
const progressFull = player.querySelector('.progress');
const fullscreen = player.querySelector('button[name="fullscreen"]');

progress.style.flexBasis = "1%";

//function
const playPauseVideo = function() {
  if(video.paused){
    video.play();
    playButton.innerHTML = "❚❚";
  } else {
    video.pause();
    playButton.innerHTML = "►";
  }
}

function setVolume(){
  video.volume = this.value;
}

function setPlaybackRate(){
  video.playbackRate = this.value;
}

function skip(){
  video.currentTime += 10;
}

function back() {
  video.currentTime -= 10;
}

function handleProgress(){
  const percent = (video.currentTime / video.duration) * 100;
  $(progress).css('flex-basis', `${percent}%`);
}

function scrub(e) {
  const timePercent = (e.offsetX / $(progressFull).width()) * 100;
  $(progress).css('flex-basis', `${timePercent}%`);
  video.currentTime = (timePercent / 100) * video.duration;
}

function setFullscreen(){
  if(video.requestFullscreen){
    video.requestFullscreen();
  } else if(video.mozRequestFullscreen){
    video.mozRequestFullscreen();
  } else if(video.webkitRequestFullscreen){
    video.webkitRequestFullscreen();
  }
}


//event listener
//play or pause video
$(playButton).click(playPauseVideo);
$(window).keypress(function(){
  if(event.which === 32) playButton.click();
});
$(video).click(function(e){
  playButton.click();
});

//increse decrese volume
$(volume).change(setVolume);
$(volume).mousemove(setVolume);

//play back rate
$(playBackRate).change(setPlaybackRate);
$(playBackRate).mousemove(setPlaybackRate);

//skip or back
$(back10s).click(back);
$(skip10s).click(skip);

//time update
$(video).bind('timeupdate', handleProgress);

//progress
let mouseDown = false;
$(progressFull).click(scrub);
$(progressFull).mousemove((e) => {
  if(mouseDown){
    scrub(e);
  }
});
$(progressFull).mousedown(function(){
  mouseDown = true;
});
$(progressFull).mouseup(function() {
  mouseDown = false;
});

//fullscreen
$(fullscreen).click(setFullscreen);
console.log(fullscreen);
