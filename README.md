# README #

This repo contains my learning project with javascript30.com

## Screenshot ##

### Drum Kit ###
![alt text](https://bytebucket.org/rifan_syah/javascript30-project/raw/8d6b3c0ff894a7e590ac6472a07667035b8d9c70/Screenshot/Drum%20Kit.gif)

### Flex Panels Image Gallery ###
![alt text](https://bytebucket.org/rifan_syah/javascript30-project/raw/5a526d0c3784253c58714e661eae3ca732786a9a/Screenshot/Flex%20Panels%20Image%20Gallery.gif)

### Type Ahead ###
![alt text](https://bytebucket.org/rifan_syah/javascript30-project/raw/ecb7f5ddae36728dc380437edefe42f57d6973cf/Screenshot/type%20ahead.gif)

### Canvas ###
![alt text](https://bytebucket.org/rifan_syah/javascript30-project/raw/f95ba5e0295bf09aa4d42617ba59680c21765599/Screenshot/Canvas.gif)

### HTML5 Video Player ###
![alt text](https://bytebucket.org/rifan_syah/javascript30-project/raw/374f91640ee9a57a47b12b583d116d0ce4151158/Screenshot/HTML%20Video.gif)